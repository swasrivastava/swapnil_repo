package com.example.springboot;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class HelloController {
	@RequestMapping("/")
	public String index() {
		return "Swapnil Says Hello Zions!!!\n";
	}


	@PostMapping(path = "/document/upload")
	public ResponseEntity<Object> uploadRegistrationDoc(@RequestHeader HttpHeaders httpHeaders,
																											@RequestPart("files") MultipartFile[] files, @RequestParam("workflowId") String workflowId) throws Exception {

		//return new ResponseEntity<>(regDocService.uploadDocumentToS3(files, workflowId), HttpStatus.OK);
		try {
			File file = null ;
			for(MultipartFile multipartFile : files) {
				file = convertMultiPartFileToFile(multipartFile);
			}
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");

			Map<String, String> mimeTypes = new HashMap<String, String>();
			mimeTypes.put("jpeg", "image/jpeg");
			mimeTypes.put("jpg", "image/jpeg");
			mimeTypes.put("png", "image/png");
			mimeTypes.put("pdf", "application/pdf");
			mimeTypes.put("txt", "text/plain");
			String fileExtension =  file.getName().substring(file.getName().lastIndexOf(".")+1);

			return ResponseEntity.ok().headers(headers).contentLength(file.length())
					.contentType(MediaType.parseMediaType(mimeTypes.get(fileExtension))).body(resource);

		} catch (Exception e) {
			System.out.println("Error while downloading file " + e.getLocalizedMessage());
			e.printStackTrace();
		}
		return null;
	}

	private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
		final File file = new File(multipartFile.getOriginalFilename());
		try (final FileOutputStream outputStream = new FileOutputStream(file)) {
			outputStream.write(multipartFile.getBytes());
		} catch (final IOException ex) {
			System.out.println("Error converting the multi-part file to file= " + ex.getMessage());
		}
		return file;
	}
}
